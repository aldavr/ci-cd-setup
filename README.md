# Setup GitLab Runner
### 1. Install GitLab Runner

Execute below code in powershell as an admin.
```powershell
cd C:/ 
docker run -d --name gitlab-runner --restart always `
    -v $PWD/GitLabRunner/config:/etc/gitlab-runner `
    -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
```

### 2. Register the runner
You need URL and registration token which you can get it from GtiLab -> Settings -> CI/CD -> Runners section.
```powershell
cd C:/GitLabRunner
docker run --rm -t -i 
    -v $PWD/GitLabRunner/config:/etc/gitlab-runner gitlab/gitlab-runner register
```
You will be prompt with:

* Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/): ***url from gitlab***
* Please enter the gitlab-ci token for this runner: ***token from gitlab***
* Please enter the gitlab-ci description for this runner: ***myRunner***
* Please enter the gitlab-ci tags for this runner (comma separated): ***myRunnerTag***
* Please enter the executor: ***docker***
* Please enter the default Docker image: ***alpine:latest***

**Volume mapping**

We can map runner's cache from docker when we use local caching.

`volumes = ["/host_mnt/c/GitLabRunner:/runnerFiles", "/c/GitLabRunner:/cache"]`

![IMAGE_DESCRIPTION](./img/gitlab-runner.png)

### 3. Setup MinIO server on Docker
*We will use MinIO server for cache storage to help increase job concurrency.*

**Installing MinIO**
* You can change '9005' port to any available port and update login credentials.

```powershell
docker run -it --restart always -p 9005:9000 `
  -e "MINIO_ACCESS_KEY=minioadmin" `
  -e "MINIO_SECRET_KEY=minioadmin" `
  --mount type=bind,source=/c/MinIO,target=/data `
  minio/minio:latest server ./data
```


*  Now you can access to MinIO  
    Endpoint: http://localhost:9005  
    RootUser: minioadmin  
    RootPass: minioadmin  


*  Create new Bucket "ProjectCacheBucket"

* Now let's update our GitLab runner config, make sure to update ServerAddress, AccessKey and BuketName.  

> This file is located C:\GitLabRunner\config\config.toml 

```toml
[runners.cache]
  Type = "s3"
  Shared = true
  [runners.cache.s3]
    ServerAddress = "127.0.0.1:9005"
    AccessKey = "minioadmin"
    SecretKey = "minioadmin"
    BucketName = "ProjectCacheBucket"
    Insecure = true
```
